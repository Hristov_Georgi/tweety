<h3 class="font-bold text-xl mb-4">Following</h3>
<ul>
    @forelse (auth()->user()->follows as $user)
    <li>
        <div >
            <a  class="flex items-center text-sm" 
                href="{{ route('profile',$user->name) }}">
                <img 
                width="40px" 
                src="{{ $user->getAvatar() }}" 
                alt="avatar"
                class="rounded-full mr-2 mb-4"
                >
                {{ $user->name }}
            </a>
        </div>
    </li>
        @empty 
            <p>You dont have any friends!</p>
    @endforelse
</ul>