<x-app>
    <form action="{{ $user->path() }}" method="POST" enctype="multipart/form-data">
        @csrf
        @method('PATCH')


         
        <div class="nb-6">
            <label class="block mb-2 uppercase font-bold text-xs text-gray-600" for="name">
                Name 
            </label>
            <input class="border border-gray-400 p-2 w-full" 
            type="text"
            id="name"
            name="name"
            value="{{ $user->name }}">

            @error('name')
                <p class="text-red-500 text-xs">{{ $message }}</p>
            @enderror
        </div>

        <div class="nb-6">
            <label class="block mb-2 uppercase font-bold text-xs text-gray-600" for="username">
                Username 
            </label>
            <input class="border border-gray-400 p-2 w-full" 
            type="text"
            id="username"
            name="username"
            value="{{ $user->username }}">

            @error('username')
                <p class="text-red-500 text-xs">{{ $message }}</p>
            @enderror
        </div>

        <div>
            <div class="nb-6">
                <label class="block mb-2 uppercase font-bold text-xs text-gray-600" for="name">
                    Avatar
                </label>
                <input class="border border-gray-400 p-2 w-full" 
                type="file"
                id="avatar"
                name="avatar"
                value="{{ $user->email }}">

                @error('avatar')
                    <p class="text-red-500 text-xs">{{ $message }}</p>
                @enderror
            </div>
            <img src="{{ $user->getAvatar() }}" alt="">
        </div>

        <div class="nb-6">
            <label class="block mb-2 uppercase font-bold text-xs text-gray-600" for="name">
                Email
            </label>
            <input class="border border-gray-400 p-2 w-full" 
            type="email"
            id="email"
            name="email"
            value="{{ $user->email }}">

            @error('email')
                <p class="text-red-500 text-xs">{{ $message }}</p>
            @enderror
        </div>

        <div class="nb-6">
            <label class="block mb-2 uppercase font-bold text-xs text-gray-600" for="name">
                Password
            </label>
            <input class="border border-gray-400 p-2 w-full" 
            type="password"
            id="password"
            name="password">

            @error('password')
                <p class="text-red-500 text-xs">{{ $message }}</p>
            @enderror
        </div>

        <div class="nb-6">
            <label class="block mb-2 uppercase font-bold text-xs text-gray-600" for="name">
                Confirm password 
            </label>
            <input class="border border-gray-400 p-2 w-full" 
            type="password"
            id="password_confirmation"
            name="password_confirmation">

            @error('password_confirmation')
                <p class="text-red-500 text-xs">{{ $message }}</p>
            @enderror
        </div>

        <div class="nb-6">
            <button class="bg-blue-400 text-white rounded py-2 px-4 mt-2" type="submit">
                Submit
            </button>
        </div>

    </form>
</x-app>