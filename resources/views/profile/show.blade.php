{{-- @extends('layouts.app')

    @section('content') --}}
    <x-app>
        <h1>{{ $user->name }}'s profile</h1>

        <header class="mb-6">
            <div class="relative">
                <img width="100%"  class="border border-gray-300 rounded-lg" 
                src="https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcSb9bf55nz8i7gwYK0G0z09-v41BrYvDfPxalYE1-Aq5A&usqp=CAU&ec=45707743" 
                alt="image">

                <img src="{{ $user->getAvatar() }}" 
                class="rounded-full mr-2  absolute bottom-0 transform -translate-x-1/2 translate-y-1/2" 
                style="left:50%" width="150" alt=""
                >
            
            </div>
            <div class="flex justify-between">
                <div>
                    <h2 class="font-bold text-xl mt-5" >{{ $user->name}}</h2>
                    <p class="text-sm">Joined {{ $user->created_at->diffForHumans() }}</p>
                </div>
            
           
                <div class="flex">
                    
                    @if (current_user()->is($user))
                    <a class="bg-blue-500 rounded-lg shadow py-2 px-2 mb-1 text-white mb-4" 
                        href="{{ $user->path('edit') }}">Edit Profile</a>
                    @endif
                    
                    <x-follow-btn :user=$user></x-follow-btn>
                </div>
                
            </div>
                <p class="text-xs mt-5">Lorem ipsum dolor sit amet consectetur, adipisicing elit. Iure modi vero, temporibus a velit ducimus iste voluptatibus officiis molestias tempore fuga voluptatum. Nemo, libero est quibusdam, sed corrupti rerum quas molestiae sit quisquam deleniti voluptatibus similique nisi.</p>
        </header>

        @include('_timeline-panel' , ['timeline' => $user->tweets])
    </x-app>
        {{-- @endsection --}}