

@unless ($user->is(current_user()))    


    <form method="POST" action="/profiles/{{ $user->name }}/follow">
        @csrf
        <button class="bg-blue-500 rounded-lg shadow py-2 px-2 text-white" 
        type="submit">{{ auth()->user()->isFollowing($user) ? 'Unfollow' : 'Follow' }} </button>
    </form>


@endunless
