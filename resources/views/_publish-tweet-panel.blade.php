<div class="border border-blue-400 rounded-lg p-6 py-4 mb-8">
    <form action="/tweet" method="POST">
        @csrf
        <textarea 
            name="content" 
            placeholder="share your toughts" 
            id="" 
            
            class="w-full">
        </textarea>
        <br class="my-4">

        @error('content')
            <p class="text-sm text-red-600"> {{ $message }}</p>
        @enderror
        <footer class="flex justify-between">
            <img 
                width="40px" 
                src="{{  auth()->user()->getAvatar() }}" 
                alt="avatar"
                class="rounded-full mr-2 mb-4"
            >
            
            <button type="submit" 
            class="bg-blue-500 rounded-lg shadow py-2 px-2 text-white"
            >Publish!</button>
        </footer>
    </form> 
</div>
