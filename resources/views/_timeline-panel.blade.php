<div class="border border-gray-300 rounded-lg">

    @forelse ($timeline as $tweet)
        {{-- @include('__tweet') --}}
        <div class="flex p-4 border-b border-b-gray-400">
            
                <div class="mr-2 flex-shrink-0">
                    <a href="{{ $tweet->author->path()}}">
                        <img 
                        width="50px" 
                        src={{ $tweet->author->getAvatar() }}
                        alt="avatar"
                        class="rounded-full mr-2 mb-4"
                        >
                    </a>
                </div>
            
            <div>
        
                <a href="{{ $tweet->author->path() }}">
                    <h5 class="font-bold mb-4">{{ $tweet->author->name}}</h5>
                </a>
                <p class="text-sm">{{ $tweet->body }}</p>
            
            </div>
        </div>

        @empty
            <li>You dont have any tweets yet!</li>
   @endforelse

</div>