<?php

namespace App\Http\Controllers;

use App\Tweet;
use Illuminate\Http\Request;

class TweetController extends Controller
{
    public function index()
    {
        $timeline = auth()->user()->timeline();
        
        // dd(auth()->user()->follows->first()->path());
        // dd(auth()->user()->timeline()->first()->author->path());
    
        return view('tweet.index', compact('timeline'));
    }


    public function store(Request $request)
    {
        $tweet = new Tweet;

        $validatedData = $request->validate([
            'content' => 'required|max:255',
        ]);

        
        $tweet->body = $validatedData['content'];
        $tweet->user_id = auth()->id();
        
       
        $tweet->save();

        return redirect()->route('home');
        // return back();

    }
}
