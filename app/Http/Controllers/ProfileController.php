<?php

namespace App\Http\Controllers;

use App\User;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\Rule;

class ProfileController extends Controller
{
    public function show(User $user)
    {
        // $user = User::find($user)->first()->name;

        // dd($user);

        // dd(auth()->user()->following($user));


        // dd($user->path());

        // dd($user);

        return view('profile.show', compact('user'));
    }

    public function edit(User $user)
    {
        // abort_if($user->isNot(current_user()),403);


        $this->authorize('edit', $user);


        return view('profile.edit', compact('user'));
    }

    public function update(Request $request, User $user)
    {


        $validated = $request->validate([
            'name' => ['string', 'max:255', 'required'],
            'username' => ['string', 'alpha_dash', 'max:255', Rule::unique('users')->ignore($user)],
            'email' => ['string', 'email','required', Rule::unique('users')->ignore($user)],
            'password' => ['min:8', 'string', 'required' , 'confirmed'],
            'avatar' => ['file']
        ]);

        
        $validated['password'] = $user->hashPass($validated['password']);

      
        
        if (isset($validated['avatar'])) {
            $validated['avatar'] = $validated['avatar']->store('avatars');
        }
        
        $user->update($validated);
        
        
        return redirect($user->path());
    }
}
