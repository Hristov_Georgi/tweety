<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\DB;


class User extends Authenticatable
{
    use Notifiable, Followable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'avatar', 'username', 'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];



    public function getRouteKeyName()
    {

        return 'username'; //TODO 
    }

    //$user->password = 'foobar';
    public function setPasswordAttribute($pass)
    {
        $this->attributes['password']=bcrypt($pass);


    }



    public function hashPass($pass)
    {
        // dd($pass = bcrypt($pass));
        return bcrypt($pass);
    }

    public function getAvatar()
    {
        if (!$this->avatar) {
            return asset('images/default_avatar.webp');
        }

        return asset('storage/' . $this->avatar);
    }

    public function timeline()
    {
        $followersIds = $this->follows()->get()->pluck('id');

        // dd($this->follows()->pluck('id'));


        // $followersIds->push($this->id);

        // return Tweet::whereIn('user_id', $followersIds)->latest()->get();

        DB::enableQueryLog(); // Enable query log
        // Your Eloquent query executed by using get()

        // Tweet::where('user_id', $this->id)->whereIn('user_id', $followersIds)->latest()->get(); //doestn work
        return Tweet::whereIn('user_id', $followersIds)->orWhere('user_id', $this->id)->latest()->get(); //works

        dd(DB::getQueryLog()); // Show results of log
    }

    public function tweets()
    {
        return $this->hasMany(Tweet::class);
    }

    public function path($append = "")
    {
        $path = route('profile', $this->username); //TODO 

        return $path . '/' . $append;
    }
}
