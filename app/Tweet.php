<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tweet extends Model
{
    public function author()
    {
        return $this->belongsTo(User::class, 'user_id');
    }
}
