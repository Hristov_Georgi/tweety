<?php

namespace App\Policies;

use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class UserPolicy
{
    use HandlesAuthorization;


    public function edit(User $currentUser, User $user)
    {
        
        // ddd($currentUser->id . '  :  ' . $user->id);
        // dd(current_user()->id . '  :  ' . $user->id);

        return $currentUser->is($user);

        // return auth()->user()->is($user);
    }

}
