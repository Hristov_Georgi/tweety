<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


Route::middleware('auth')->group(function(){
    Route::get('/tweet', 'TweetController@index')->name('home');
    Route::post('/tweet', 'TweetController@store');
    
    Route::post('/profiles/{user}/follow', 'FollowController@store');
    Route::get('/profile/{user}/edit', 'ProfileController@edit');
    Route::patch('/profile/{user}', 'ProfileController@update');


    Route::get('/explore', 'ExplorerController@index')->name('explore');
 
    Route::get('/logout', function(){
        Auth::logout();
        return redirect()-route('home');
    })->name('logout');

});


// Route::get('tweet/profile/{user}', 'ProfileController@show')->name('profile'); old ...
Route::get('profile/{user}', 'ProfileController@show')->name('profile');



Auth::routes();



